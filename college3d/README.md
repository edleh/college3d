# Collège 3D

* **Website:** http://pascal.peter.free.fr/college3d.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2003-2021

----


#### Les outils utilisés pour développer Collège 3D - The tools used to develop Collège 3D
* [three.js](https://threejs.org)
* [Blender](https://www.blender.org/)
* [Bootstrap](https://getbootstrap.com/)
* [Bootstrap Icons](https://icons.getbootstrap.com/)


#### Code source - Source code
* [GitLab](https://gitlab.com/edleh/college3d)


#### Licence - License
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html)
