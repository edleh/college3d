/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    GAME SCENARIOS
    --------------------------------------------------------------

    Definitions of games, levels and missions
         * A game is composed of levels;
         * One level is composed of missions.

    STRUCTURE OF A GAME:
        'Short name of the game (internal)': {
            name: 'full name of the game (displayed)', 
            levels: number of levels, 
            loop: true,                             // optional - true if the levels run in a loop
            x: {},                                  // one dictionary per level
            }
    STRUCTURE OF A LEVEL:
        index of the level: {
            name: 'name of the level', 
            comment: 'explanations displayed in the menu', 
            positions: [[10, 20, -40],],            // lists of positions where the character can appear - at least one
            forcePosition: true,                    // optional - true if a position must be chosen
            missions: [],                           // list of missions - one dictionary per mission

            random: true,                           // optional - true if the missions are executed at random
            pass: 2,                                // optional - 2 if second pass without the visible target ("discover" game)

            weather: 'good weather',                // optional - not implemented - level weather
            }
    STRUCTURE OF A MISSION:
        {
            text: 'Go to the gate.',                // the displayed text (order to be executed by the player)
            target: [[10, 10, -36], 3, 'objets/divers/cible'], 
                                                    // the target to be reached. List of 3 items:
                                                        * A list of possible positions for the target
                                                        * The detection radius
                                                        * The name of the object to display

            time: [30, 30, 60, 20],                 // optional - used if time is limited.
                                                    2 values (or 4 values if pass = 2):
                                                        * The first indicates the duration (will be used as a countdown)
                                                        * The second indicates from when the target is displayed

            hook: 'objets/saisons/bouquet',         // optional - not implemented
                                                    If the character holds an object by hand during the mission
            head: 'objets/10ans/chapeau2_head',     // optional - not implemented
                                                    If the character carries an object on the head
            result: 'objets/10ans/affiche1',        // optional - not implemented
                                                    If an object is created at the end
            }
    --------------------------------------------------------------
    SCÉNARIOS DU JEU
    --------------------------------------------------------------

    Définitions des jeux, niveaux et missions
        * un jeu est composé de niveaux ;
        * un niveau est composé de missions.

    STRUCTURE D'UN JEU :
        'nom court du jeu (interne)': {
            name: 'nom complet du jeu (affiché)', 
            levels: nombre de niveaux, 
            loop: true,                             // facultatif - true si les niveaux s'exécutent en boucle
            x: {},                                  // un dictionnaire par niveau
            }
    STRUCTURE D'UN NIVEAU :
        indice du niveau: {
            name: 'nom du niveau', 
            comment: 'explications affichées dans le menu', 
            positions: [[10, 20, -40], ],           // listes des positions où le personnage pourra apparaitre - au moins une
            forcePosition: true,                    // facultatif - true si une position doit être choisie
            missions: [],                           // liste des missions - un dictionnaire par mission

            random: true,                           // facultatif - true si les missions sont executées au hasard
            pass: 2,                                // facultatif - 2 si deuxième passage sans la cible visible (jeu "discover")

            weather: 'good weather',                // facultatif - pas implémenté - la météo du niveau
            }
    STRUCTURE D'UNE MISSION :
        {
            text: 'Go to the gate.',                // le texte affiché (ordre à executer par le joueur)
            target: [[[10, 10, -36], ], 3, 'objets/divers/cible'], 
                                                    // la cible à atteindre. Liste de 3 éléments :
                                                        * une liste des positions possibles pour la cible
                                                        * le rayon de détection
                                                        * le nom de l'objet à afficher

            time: [30, 30, 60, 20],                 // facultatif - utilisé si le temps est limité.
                                                    2 valeurs (ou 4 valeurs si pass = 2) :
                                                        * la première indique la durée (servira de décompte)
                                                        * la deuxième indique à partir de quand on affiche la cible

            hook: 'objets/saisons/bouquet',         // facultatif - pas implémenté
                                                    si le personnage tient un objet à la main pendant la mission
            head: 'objets/10ans/chapeau2_head',     // facultatif - pas implémenté
                                                    si le personnage porte un objet sur la tête
            result: 'objets/10ans/affiche1',        // facultatif - pas implémenté
                                                    si un objet est créé à la fin
            }
    --------------------------------------------------------------
*/





var GAMES = {
    /*
    the list of games (description above)
    ----------
    la liste des jeux (description ci-dessus)
    */


    'free': {
        //GAMES[PARAMS['what']]
        name: 'Free visit', 
        levels: 0, 
        }, 


    'discover': {
        //GAMES[PARAMS['what']]
        name: 'Discover the college', 
        levels: 4, 
        0: {
            //GAMES[PARAMS['what']][PARAMS['level']]
            name: 'Before Enter', 
            comment: "You'll find different places to college entrance.||Initially a marker will help you.", 
            positions: [[10, 20, -40], ], 
            missions: [
                {
                    text: 'Go to the gate.', 
                    target: [[[10, 10, -36], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                {
                    text: 'Go under the bike garage.', 
                    target: [[[-4, 9, -40], ], 5, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                {
                    text: 'Go to the front door.', 
                    target: [[[11, 9, -21], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                ], 
            random: true, 
            pass: 2, 
            weather: 'good weather', 
            }, 
        1: {
            name: 'The Hall', 
            comment: 'Everything happens this time under the hall.', 
            positions: [[11, 10, -13], ], 
            missions: [
                {
                    text: 'Go between the 2 fountains.', 
                    target: [[[13, 9, -1], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                {
                    text: 'Go in front of the reception.', 
                    target: [[[6, 9, -10], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                {
                    text: 'Go to the exit of the refectory.', 
                    target: [[[17, 9.5, -18], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                ], 
            random: true, 
            pass: 2, 
            weather: 'good weather', 
            }, 
        2: {
            name: 'Near the tower', 
            comment: 'Because there is a tower in the school yard.', 
            positions: [[14, 11, 9], ], 
            missions: [
                {
                    text: 'Go between the ping-pong tables.', 
                    target: [[[16, 9, 22], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                {
                    text: 'Go to the tower.', 
                    target: [[[38, 10, 42], ], 6, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                {
                    text: 'Go to the entrance of the cafeteria.', 
                    target: [[[22, 9, 4], ], 3, 'objets/divers/cible'], 
                    time: [30, 30, 60, 20]}, 
                ], 
            random: true, 
            pass: 2, 
            weather: 'good weather', 
            }, 
        3: {
            name: 'The buildings', 
            comment: 'You will know the building entrances.', 
            positions: [[-4, 11, 9], ], 
            missions: [
                {
                    text: 'Go under the small hall.|(building A)', 
                    target: [[[-13, 9, -5], ], 3, 'objets/divers/cible'], 
                    time: [45, 45, 60, 20]}, 
                {
                    text: 'Go to the stairs of building A.', 
                    target: [[[-42, 8, -11], ], 3, 'objets/divers/cible'], 
                    time: [45, 45, 60, 20]}, 
                {
                    text: 'Go to the entrance of Building B.', 
                    target: [[[-33, 7, 15], ], 3, 'objets/divers/cible'], 
                    time: [45, 45, 60, 20]}, 
                {
                    text: 'Go to the entrance of the CDI.|(building C)', 
                    target: [[[-5, 9, 22], ], 3, 'objets/divers/cible'], 
                    time: [45, 45, 60, 20]}, 
                {
                    text: 'Go to the entrance of the technology rooms.|(building C)', 
                    target: [[[5, 9, 49], ], 3, 'objets/divers/cible'], 
                    time: [45, 45, 60, 20]}, 
                ], 
            random: true, 
            pass: 2, 
            weather: 'good weather', 
            }, 
        }, 


    'seasons': {
        name: 'During seasons', 
        }, 


    '2003-2013': {
        name: '2003-2013', 
        }, 


    'exhibition': {
        name: 'The exhibition', 
        levels: 2, 
        loop: true, 
        0: {
            name: 'Explanations', 
            comment: "You still did not know what's inside the tower.||A secret building is hidden there.", 
            positions: [[10, 20, -40], ], 
            forcePosition: true, 
            missions: [
                {
                    text: 'The exhibition takes place in the tower.', 
                    target: [[[38, 10, 42], ], 1, '']}, 
                ], 
            }, 
        1: {
            name: '', 
            comment: '', 
            positions: [[36, -3, 36], ], 
            forcePosition: true, 
            missions: [
                {
                    text: '', 
                    target: [[[33.5, -3, 41], ], 1, '']}, 
                ], 

            weather: 'good weather', 
            }, 
        }, 


    };





Game = function() {
    /*
    the game object (handles the scenario)
    ----------
    l'objet jeu (gère le scénario)
    */


    this.state = 'DISABLED';

    this.mission = undefined;
    this.target = undefined;
    this.targetPosition = [];
    this.missions = [];
    this.timer = [-1];


    this.init = function(level) {
        // implementation of the scenario
        // ----------
        // mise en place du scénario
        //console.log('init level :' + level);

        function shuffle(array) {
            // to manage the list of random missions
            // ----------
            // pour gérer la liste des missions au hasard
            // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
            var currentIndex = array.length, temporaryValue, randomIndex;
            // While there remain elements to shuffle...
            while (0 !== currentIndex) {
                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
                }
            return array;
            };

        if (PARAMS['level'] != level)
            PARAMS['level'] = level;
        if (level > SCORES[PARAMS['what']])
            SCORES[PARAMS['what']] = level;
        var positions = GAMES[PARAMS['what']][level]['positions'];
        var initialPosition = positions[Math.floor(Math.random() * positions.length)];
        var missions = GAMES[PARAMS['what']][level]['missions'];
        this.missions = [];
        if (GAMES[PARAMS['what']][level].pass === undefined)
            GAMES[PARAMS['what']][level].pass = 1;
        if (GAMES[PARAMS['what']][level].random === true) {
            var temp = new Array();
            for (var j=0; j < missions.length; j++)
                temp.push(j);
            temp = shuffle(temp);
            //console.log(temp);
            for (var i=0; i < GAMES[PARAMS['what']][level].pass; i++)
                for (var j=0; j < missions.length; j++)
                    this.missions.push(1000 * i + temp[j]);
            }
        else {
            for (var i=0; i < GAMES[PARAMS['what']][level].pass; i++)
                for (var j=0; j < missions.length; j++)
                    this.missions.push(1000 * i + j);
            }
        this.next();
        return initialPosition;
        };


    this.update = function(timer) {
        // update of the state of the game
        // ----------
        // mise à jour de l'état du jeu
        if (this.timer[0] < -1)
            quitGame();
        if (this.timer.length > 1 && timer === true)
            this.timer[0] -= 1;
        if (this.state == 'LOST')
            return;
        else if (this.timer.length > 1) {
            if (this.timer[0] < 1) {
                this.state = 'LOST';
                INFOS.info = tr('YOU LOST!');
                CHRONO['5'] = TIMER;
                return;
                }
            else if (this.timer[0] < this.timer[1]) {
                this.target.position.set(
                    this.targetPosition[0], 
                    this.targetPosition[1], 
                    this.targetPosition[2]);
                this.timer[1] = -1;
                }
            }

        var x = CHARACTER.root.position.x;
        var y = CHARACTER.root.position.y;
        var z = CHARACTER.root.position.z;
        var xx = this.targetPosition[0];
        var yy = this.targetPosition[1];
        var zz = this.targetPosition[2];
        var d = Math.sqrt((x - xx)*(x - xx) + (y - yy)*(y - yy) + (z - zz)*(z - zz));
        if (d < this.mission.target[1])
            this.next();
        };


    this.next = function() {
        // next mission (or end of level or game)
        // ----------
        // mission suivante (ou fin du niveau)
        var i = this.missions.shift();
        //console.log(this.missions + ' -> ' + i);
        if (i === undefined) {
            var level = parseInt(PARAMS['level']) + 1;
            if (level < GAMES[PARAMS['what']].levels) {
                initialPosition = this.init(level);
                if (GAMES[PARAMS['what']][level].forcePosition) {
                    //console.log('initialPosition:' + initialPosition);
                    CHARACTER.root.position.set(
                        initialPosition[0], 
                        initialPosition[1], 
                        initialPosition[2]);
                    }
                }
            else if (GAMES[PARAMS['what']].loop) {
                initialPosition = this.init(0);
                if (GAMES[PARAMS['what']][0].forcePosition) {
                    //console.log('initialPosition:' + initialPosition);
                    CHARACTER.root.position.set(
                        initialPosition[0], 
                        initialPosition[1], 
                        initialPosition[2]);
                    }
                }
            else {
                this.target.position.set(0, -100, 0);
                this.state = 'WON';
                INFOS.info = tr('YOU FINISHED THIS GAME!');
                CHRONO['5'] = TIMER;
                }
            return;
            }
        else {
            if (i > 999)
                this.mission = GAMES[PARAMS['what']][PARAMS['level']]['missions'][i - 1000];
            else
                this.mission = GAMES[PARAMS['what']][PARAMS['level']]['missions'][i];
            var positions = this.mission.target[0];
            this.targetPosition = positions[Math.floor(Math.random() * positions.length)];
            if (this.mission.time == undefined)
                this.mission.time = [];
            if (i > 999 && this.mission.time.length > 2) {
                this.timer = [];
                this.timer.push(this.mission.time[2]);
                this.timer.push(this.mission.time[3]);
                }
            else if (this.mission.time.length > 0) {
                this.timer = [];
                this.timer.push(this.mission.time[0]);
                this.timer.push(this.mission.time[1]);
                }
            else
                this.timer = [-1];
            }

        //console.log(this.mission);
        if (this.target !== undefined) {
            if (i > 999)
                this.target.position.set(0, -100, 0);
            else
                this.target.position.set(
                    this.targetPosition[0], 
                    this.targetPosition[1], 
                    this.targetPosition[2]);
            }
        showMessage(
            GAMES[PARAMS['what']][PARAMS['level']]['name'], 
            parseInt(PARAMS['level']) + 1, 
            this.mission.text);
        };

    };

