/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    INPUT MANAGEMENT (KEYBOARD, MOUSE, TOUCHSCREEN, GAMEPAD)
    THE DICTIONARY inputState RETURNS THE STATUS OF ACTIONS
    --------------------------------------------------------------
    GESTION DES ENTRÉES (CLAVIER, SOURIS, ÉCRAN TACTILE, MANETTE)
    LE DICTIONNAIRE inputState RETOURNE L'ÉTAT DES ACTIONS
    --------------------------------------------------------------
*/





function buttonClickEsc() {
    controls.inputState.QUIT = true;
    }

function buttonClickP() {
    controls.inputState.PAUSE = true;
    }

function buttonClickM() {
    controls.inputState.MESSAGE = true;
    }

function buttonClickC() {
    controls.inputState.CAMERA = true;
    }

function quitGame() {
    if (PARAMS['what'] == 'free')
        launchPage('index');
    else
        launchPage('levels');
    }





THREE.InputControls = function(domElement, control) {

    this.domElement = (domElement !== undefined) ? domElement: document;
    this.control = (control !== undefined) ? control: 'keyboard';

    this.enabled = false;

    var controlX = controlY = viewHalfX = viewHalfY = 0;

    this.inputState = {
        QUIT: false, 
        MESSAGE: false, 
        PAUSE: false, 
        
        PLAYER_MUST_ADVANCE: true, 
        PLAYER_ADVANCE: 0, 
        PLAYER_TURN: 0, 
        PLAYER_JUMP: false, 
        
        CAMERA: false, 
        CAMERA_TOUR: false, 
        };

    this.handleResize = function() {
        if (this.domElement === document) {
            viewHalfX = window.innerWidth / 2;
            viewHalfY = window.innerHeight / 2;
            }
        else {
            viewHalfX = this.domElement.offsetWidth / 2;
            viewHalfY = this.domElement.offsetHeight / 2;
            }
        };





    /*
    *****************************************
    KEYBOARD
    ----------
    CLAVIER
    http://asquare.net/javascript/tests/KeyCode.html
    *****************************************
    */

    this.onKeyDown = function(event) {
        if (this.enabled === false)
            return;
        //console.log('onKeyDown');
        event.stopPropagation();
        if (this.control == 'keyboard') {
            switch(event.keyCode) {
                case 38: /*up*/
                case 87: /*W*/          this.inputState.PLAYER_ADVANCE = 1; break;
                case 40: /*down*/
                case 83: /*S*/          this.inputState.PLAYER_ADVANCE = -1; break;
                case 37: /*left*/
                case 65: /*A*/          this.inputState.PLAYER_TURN = 1; break;
                case 39: /*right*/
                case 68: /*D*/          this.inputState.PLAYER_TURN = -1; break;
                case 32: /*space*/      this.inputState.PLAYER_JUMP = true; break;

                case 67: /*C*/          this.inputState.CAMERA = true; break;
                case 27: /*esc*/        this.inputState.QUIT = true; break;
                case 77: /*M*/        this.inputState.MESSAGE = true; break;
                case 80: /*P*/        this.inputState.PAUSE = true; break;
                case 84: /*T*/        this.inputState.CAMERA_TOUR = true; break;
                }
            }
        else {
            switch(event.keyCode) {
                case 67: /*C*/          this.inputState.CAMERA = true; break;
                case 27: /*esc*/        this.inputState.QUIT = true; break;
                case 77: /*M*/        this.inputState.MESSAGE = true; break;
                case 80: /*P*/        this.inputState.PAUSE = true; break;
                case 84: /*T*/        this.inputState.CAMERA_TOUR = true; break;
                }
            }
        };

    this.onKeyUp = function(event) {
        if (this.enabled === false)
            return;
        event.stopPropagation();
        if (this.control == 'keyboard') {
            switch(event.keyCode) {
                case 38: /*up*/
                case 87: /*W*/          this.inputState.PLAYER_ADVANCE = 0; break;
                case 40: /*down*/
                case 83: /*S*/          this.inputState.PLAYER_ADVANCE = 0; break;
                case 37: /*left*/
                case 65: /*A*/          this.inputState.PLAYER_TURN = 0; break;
                case 39: /*right*/
                case 68: /*D*/          this.inputState.PLAYER_TURN = 0; break;
                }
            }
        };





    /*
    *****************************************
    MOUSE
    ----------
    SOURIS
    *****************************************
    */

    this.onContextMenu = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        };

    this.onMouseDown = function(event) {
        if (this.enabled === false)
            return;
        if (this.domElement !== document)
            this.domElement.focus();
        event.preventDefault();
        event.stopPropagation();
        switch (event.button) {
            case 0: {
                // necessary with the ascii effect :
                if (viewHalfY < 10)
                    this.handleResize();
                if (this.domElement === document) {
                    controlX = event.pageX - viewHalfX;
                    controlY = event.pageY - viewHalfY;
                    }
                else {
                    controlX = event.pageX - this.domElement.offsetLeft - viewHalfX;
                    controlY = event.pageY - this.domElement.offsetTop - viewHalfY;
                    }
                var deltaX = - 2 * controlX / viewHalfX;
                this.inputState.PLAYER_TURN = deltaX;
                this.inputState.PLAYER_MUST_ADVANCE = false;
                var deltaY = - 2 * controlY / viewHalfY;
                if (deltaY < -1.5)
                    this.inputState.PLAYER_ADVANCE = -1;
                else
                    this.inputState.PLAYER_ADVANCE = 1;
                break;
                }
            case 1: this.inputState.CAMERA = true; break;
            case 2: this.inputState.PLAYER_JUMP = true; break;
            }
        };

    this.onMouseMove = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        if (this.inputState.PLAYER_ADVANCE == 0)
            return;
        if (this.domElement === document)
            controlX = event.pageX - viewHalfX;
        else
            controlX = event.pageX - this.domElement.offsetLeft - viewHalfX;
        this.inputState.PLAYER_MUST_ADVANCE = true;
        var deltaX = - 2 * controlX / viewHalfX;
        if (deltaX < -0.3 || deltaX > 0.3)
            this.inputState.PLAYER_TURN = deltaX;
        else
            this.inputState.PLAYER_TURN = 0;
        };

    this.onMouseUp = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        switch (event.button) {
            case 0: {
                this.inputState.PLAYER_ADVANCE = 0;
                break;
                }
            }
        };

    this.onMouseWheel = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        if (event.deltaY != 0)
            this.inputState.CAMERA = true;
        };





    /*
    *****************************************
    TOUCHPAD
    ----------
    ÉCRAN TACTILE
    *****************************************
    */

    this.onTouchStart = function(event) {
        if (this.enabled === false)
            return;
        if (this.domElement !== document)
            this.domElement.focus();
        event.preventDefault();
        event.stopPropagation();
        switch (event.touches.length) {
            case 1: {
                // necessary with the ascii effect :
                if (viewHalfY < 10)
                    this.handleResize();
                if (this.domElement === document) {
                    controlX = event.touches[0].pageX - viewHalfX;
                    controlY = event.touches[0].pageY - viewHalfY;
                    }
                else {
                    controlX = event.touches[0].pageX - this.domElement.offsetLeft - viewHalfX;
                    controlY = event.touches[0].pageY - this.domElement.offsetTop - viewHalfY;
                    }
                var deltaX = - 2 * controlX / viewHalfX;
                this.inputState.PLAYER_TURN = deltaX;
                this.inputState.PLAYER_MUST_ADVANCE = false;
                var deltaY = - 2 * controlY / viewHalfY;
                if (deltaY < -1.5)
                    this.inputState.PLAYER_ADVANCE = -1;
                else
                    this.inputState.PLAYER_ADVANCE = 1;
                break;
                }
            case 2: this.inputState.PLAYER_JUMP = true; break;
            case 3: this.inputState.CAMERA = true; break;
            }
        };

    this.onTouchMove = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        if (this.domElement === document)
            controlX = event.touches[0].pageX - viewHalfX;
        else
            controlX = event.touches[0].pageX - this.domElement.offsetLeft - viewHalfX;
        this.inputState.PLAYER_MUST_ADVANCE = true;
        var deltaX = - 2 * controlX / viewHalfX;
        if (deltaX < -0.3 || deltaX > 0.3)
            this.inputState.PLAYER_TURN = deltaX;
        else
            this.inputState.PLAYER_TURN = 0;
        };

    this.onTouchEnd = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        switch (event.touches.length) {
            case 1: {
                this.inputState.PLAYER_ADVANCE = 0;
                break;
                }
            }
        };





    /*
    *****************************************
    GAMEPAD
    ----------
    MANETTE
    *****************************************
    */

    function findGamepad(id) {
        var gamepads = navigator.getGamepads();
        if (gamepads.length > 0) {
            if (id < gamepads.length)
                return gamepads[id];
            }
        }

    this.update = function() {
        if (this.enabled === false)
            return;

        if (this.control == 'gamepad') {
            var gamepad = findGamepad(0);
            if (gamepad !== undefined) {
                this.inputState.PLAYER_ADVANCE = - gamepad.axes[1];
                this.inputState.PLAYER_TURN = - gamepad.axes[0];
                for (var i=0; i < gamepad.buttons.length; i++)
                    if (gamepad.buttons[i].pressed)
                        switch (i) {
                            case 0: this.inputState.PLAYER_JUMP = true; break;
                            case 1: this.inputState.CAMERA = true; break;
                            case 2: this.inputState.MESSAGE = true; break;
                            case 4: this.inputState.QUIT = true; break;
                            case 5: this.inputState.PAUSE = true; break;
                            default: this.inputState.PLAYER_JUMP = true;
                            }
                }
            }
        };





    /*
    *****************************************
    INSTALLATION
    ----------
    MISE EN PLACE
    *****************************************
    */

    var _onKeyDown = bind(this, this.onKeyDown);
    window.addEventListener('keydown', _onKeyDown, false);
    if (this.control == 'keyboard') {
        var _onKeyUp = bind(this, this.onKeyUp);
        window.addEventListener('keyup', _onKeyUp, false);
        }
    else if (this.control == 'mouse') {
        var _onContextMenu = bind(this, this.onContextMenu);
        this.domElement.addEventListener('contextmenu', _onContextMenu, false)
        var _onMouseDown = bind(this, this.onMouseDown);
        this.domElement.addEventListener('mousedown', _onMouseDown, false);
        var _onMouseMove = bind(this, this.onMouseMove);
        this.domElement.addEventListener('mousemove', _onMouseMove, false);
        var _onMouseUp = bind(this, this.onMouseUp);
        this.domElement.addEventListener('mouseup', _onMouseUp, false);
        var _onMouseWheel = bind(this, this.onMouseWheel);
        this.domElement.addEventListener('wheel', _onMouseWheel, false);
        }
    else if (this.control == 'touch') {
        var _onTouchStart = bind(this, this.onTouchStart);
        this.domElement.addEventListener('touchstart', _onTouchStart, false);
        var _onTouchMove = bind(this, this.onTouchMove);
        this.domElement.addEventListener('touchmove', _onTouchMove, false);
        var _onTouchEnd = bind(this, this.onTouchEnd);
        this.domElement.addEventListener('touchend', _onTouchEnd, false);
        }


    function bind(scope, fn) {
        return function() {
            fn.apply(scope, arguments);
            };
        }


    this.dispose = function() {
        window.removeEventListener('keydown', _onKeyDown, false);
        if (this.control == 'keyboard')
            window.removeEventListener('keyup', _onKeyUp, false);
        else if (this.control == 'mouse') {
            this.domElement.removeEventListener('contextmenu', _onContextMenu, false);
            this.domElement.removeEventListener('mousedown', _onMouseDown, false);
            this.domElement.removeEventListener('mousemove', _onMouseMove, false);
            this.domElement.removeEventListener('mouseup', _onMouseUp, false);
            this.domElement.removeEventListener('wheel', _onMouseWheel, false);
            }
        else if (this.control == 'touch') {
            this.domElement.removeEventListener('touchstart', _onTouchStart, false);
            this.domElement.removeEventListener('touchmove', _onTouchMove, false);
            this.domElement.removeEventListener('touchend', _onTouchEnd, false);
            }
        };



    this.handleResize();
    };

