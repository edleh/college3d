/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    MANAGEMENT OF CHARACTERS
    (movements, collisions, animations)
    --------------------------------------------------------------
    GESTION DU PERSONNAGE
    (déplacements, collisions, animations)
    --------------------------------------------------------------
*/





THREE.Character = function() {

    this.enabled = false;

    // base object (root)
    // ----------
    // objet de base (root)
    var geometry = new THREE.BoxGeometry(0.1, 2, 0.5);
    var material = new THREE.MeshLambertMaterial(
        {color: 0x8888ff, shading: THREE.FlatShading, wireframe:true});
    material.visible = false;
    this.root = new THREE.Mesh(geometry, material);

    // travel and actions
    // ----------
    // déplacements et actions
    var quit = {lastTime: 0};
    var pause = {lastTime: 0};
    var message = {lastTime: 0};
    this.advance = {direction: 0, speed: 0, min: 3, max: 10};
    var turn = {speed: Math.PI / 2, value: 0, direction: 0, max: 0};
    var jump = {lastTime: 0, value: 0, speed: 3, max: 2};
    var cameraSwitch = {
        lastTime: 0, 
        actual: 0, 
        positions: {
            0: new THREE.Vector3(0, 1, 4), 
            1: new THREE.Vector3(0, 0.1, 0)}
        };
    var cameraTour = {timer: 0, duration: 3}
    var lastPosition = new THREE.Vector3();
    var raycaster = new THREE.Raycaster();
    this.oldPosition = {x: 0, z: 0};

    // character, animations and sounds
    // ----------
    // personnage, animations et sons
    this.mesh = undefined;
    this.mixer = undefined;
    var action = 'no';
    this.animations = {
        'LIST': [], 
        'no': -1, 
        'wait': -1, 
        'walk': -1, 
        'jump': -1, 
        };
    var jump_sound, jingle;
    var cameraTarget;





    this.init = function(data) {
        this.root.position.set(
            data.position[0], data.position[1], data.position[2]);
        lastPosition.copy(this.root.position);
        if (data.rotationY !== undefined)
            this.root.rotateY(data.rotationY);

        camera.position.copy(
            cameraSwitch.positions[0]);
        cameraTarget = new THREE.Object3D();
        cameraTarget.position.set(0, 0.2, 0);
        this.root.add(cameraTarget);
        cameraTarget.add(camera);

        scene.add(this.root);

        // loading sound (if requested)
        // ----------
        // chargement des sons (si demandé)
        if (PARAMS['music'] || PARAMS['sounds']) {
            var listener = new THREE.AudioListener();
            camera.add(listener);
            var audioLoader = new THREE.AudioLoader();
            if (PARAMS['music']) {
                var sound = new THREE.Audio(listener);
                audioLoader.load('assets/sounds/Project_Utopia.ogg', function(buffer) {
                    sound.setBuffer(buffer);
                    sound.setLoop(true);
                    sound.setVolume(0.5);
                    sound.play();
                    });
                jingle = new THREE.Audio(listener);
                audioLoader.load('assets/sounds/sonnerie1.ogg', function(buffer) {
                    jingle.setBuffer(buffer);
                    jingle.setVolume(0.5);
                    jingle.play();
                    });
                }
            if (PARAMS['sounds']) {
                jump_sound = new THREE.Audio(listener);
                audioLoader.load('assets/sounds/jump.wav', function(buffer) {
                    jump_sound.setBuffer(buffer);
                    jump_sound.setVolume(0.5);
                    });
                }
            }

        quit.lastTime = TIMER;
        pause.lastTime = TIMER;
        message.lastTime = TIMER;
        jump.lastTime = TIMER;
        cameraSwitch.lastTime = TIMER;
        };


    this.update = function(delta) {
        if (this.enabled === false)
            return;
        if (DATA.LOADED < 10) {
            //console.log('CHARACTER');
            this.root.add(this.mesh);
            this.doAction('wait');
            controls.inputState.CAMERA_TOUR = true;
            DATA.LOADED = 10;
            return;
            }

        // interactions management (exit etc)
        // ----------
        // gestion des interactions (quitter etc)
        if (controls.inputState.QUIT)
            if (TIMER - quit.lastTime > 0.5) {
                controls.inputState.QUIT = false;
                quit.lastTime = TIMER;
                controls.enabled = false;
                if (confirm(tr('QUIT THE GAME ?')) == true)
                    quitGame();
                controls.enabled = true;
                return;
                }
        if (controls.inputState.MESSAGE)
            if (TIMER - message.lastTime > 0.5) {
                controls.inputState.MESSAGE = false;
                message.lastTime = TIMER;
                showMessage();
                CHRONO['5'] = TIMER;
                return;
                }
        if (controls.inputState.PAUSE)
            if (TIMER - pause.lastTime > 0.5) {
                controls.inputState.PAUSE = false;
                pause.lastTime = TIMER;

                //showModal('PAUSE');

                controls.enabled = false;
                alert(tr('PAUSE'));
                controls.enabled = true;

                return;
                }
        if (controls.inputState.CAMERA) {
            controls.inputState.CAMERA = false;
            if (TIMER - cameraSwitch.lastTime > 1) {
                cameraSwitch.lastTime = TIMER;
                cameraSwitch.actual += 1;
                if (cameraSwitch.actual > 1)
                    cameraSwitch.actual = 0;
                camera.position.copy(
                    cameraSwitch.positions[
                        cameraSwitch.actual]);
                }
            }
        if (controls.inputState.CAMERA_TOUR) {
            if (cameraTour.timer > 0) {
                cameraTour.timer += delta;
                cameraTarget.rotateY(delta * 2 * Math.PI / cameraTour.duration);
                if (cameraTour.timer > cameraTour.duration) {
                    controls.inputState.CAMERA_TOUR = false;
                    var diff = cameraTour.duration - cameraTour.timer;
                    cameraTarget.rotateY(diff * 2 * Math.PI / cameraTour.duration);
                    cameraTour.timer = 0;
                    }
                }
            else {
                cameraTour.timer += delta;
                cameraTarget.rotateY(delta * 2 * Math.PI / cameraTour.duration);
                }
            }

        // update of the character and actions
        // ----------
        // mise à jour du personnage et des actions
        this.updateMovementModel(delta);
        if (this.mixer != undefined)
            this.mixer.update(delta);
        };


    this.updateMovementModel = function(delta) {
        lastPosition.copy(this.root.position);
        var moving = false;
        var collide = false;

        // calculation of the advance
        // ----------
        // calcul de l'avancée
        var value = 0;
        if (controls.inputState.PLAYER_ADVANCE != 0) {
            if (controls.inputState.PLAYER_ADVANCE > 0)
                this.advance.direction = 1;
            else
                this.advance.direction = -1;
            this.doAction('walk');
            var min = this.advance.min * this.advance.direction * controls.inputState.PLAYER_ADVANCE;
            if (this.advance.speed < min)
                this.advance.speed = min;
            else {
                this.advance.speed += 2 * delta;
                var max = this.advance.max * this.advance.direction * controls.inputState.PLAYER_ADVANCE;
                if (this.advance.speed > max)
                    this.advance.speed = max;
                }
            if (controls.inputState.PLAYER_MUST_ADVANCE)
                value = - controls.inputState.PLAYER_ADVANCE * this.advance.speed * delta;
            }
        else if (this.advance.speed > 0) {
            this.doAction('walk');
            this.advance.speed -= 10 * delta;
            if (this.advance.speed < 0) {
                this.advance.speed = 0;
                this.doAction('wait');
                }
            if (controls.inputState.PLAYER_MUST_ADVANCE)
                value = - this.advance.direction * this.advance.speed * delta;
            }
        else {
            this.doAction('wait');
            this.advance.direction = 0;
            }
        if (value != 0) {
            this.root.translateZ(value);
            moving = true;
            }

        // calculation of rotations
        // ----------
        // calcul des rotations
        if (controls.control == 'mouse' || controls.control == 'touch') {
            if (controls.inputState.PLAYER_TURN != 0) {
                if (controls.inputState.PLAYER_TURN > 0)
                    turn.direction = 1;
                else
                    turn.direction = - 1;
                turn.max = turn.direction * controls.inputState.PLAYER_TURN * camera.aspect * Math.PI / 8;
                controls.inputState.PLAYER_TURN = 0;
                this.root.rotateY(turn.direction * turn.max * turn.speed * delta);
                turn.max -= turn.max * turn.speed * delta;
                //console.log('PLAYER_TURN :' + turn.max);
                }
            else if (turn.max > 0.01) {
                this.root.rotateY(turn.direction * turn.max * turn.speed * delta);
                turn.max -= turn.max * turn.speed * delta;
                //console.log('max :' + turn.max);
                }
            else
                turn.max = 0;
            }
        else {
            if (controls.inputState.PLAYER_TURN != 0)
                this.root.rotateY(controls.inputState.PLAYER_TURN * turn.speed * delta);
            }

        // calculation of the jump
        // ----------
        // calcul du saut
        if (jump.value > 0)
            controls.inputState.PLAYER_JUMP = false;
        if (controls.inputState.PLAYER_JUMP) {
            controls.inputState.PLAYER_JUMP = false;
            if (TIMER - jump.lastTime > 1) {
                jump.lastTime = TIMER;
                jump.value = jump.max;
                this.doAction('jump');
                if (PARAMS['sounds'])
                    jump_sound.play();
                }
            }
        if (jump.value > 0) {
            jump.value -= jump.speed * delta;
            if (jump.value < 0)
                jump.value = 0;
            else if (jump.value > 1)
                this.doAction('jump');
            }
        if (jump.value != 0) {
            this.root.translateY(jump.value);
            moving = true;
            }

        // height management in the ground
        // ----------
        // gestion de la hauteur sur le sol
        var hits, distance;
        raycaster.ray.direction.set(0, -1, 0);
        raycaster.ray.origin.copy(this.root.position);
        hits = raycaster.intersectObjects(DATA.GROUND.collidables);
        if (hits.length > 0) {
            distance = hits[0].distance;
            moving = true;
            if (distance < 1.01)
                this.root.translateY(1 - distance);
            else if (distance < 3)
                this.root.translateY(-0.1);
            else
                this.root.translateY(-1);
            }
        else {
            this.root.position.copy(lastPosition);
            }

        // collision management
        // ----------
        // gestion des collisions
        if (moving === true) {
            raycaster.ray.origin.copy(this.root.position);
            var v = new THREE.Vector2(
                this.root.position.x - lastPosition.x, 
                this.root.position.z - lastPosition.z).normalize();
            //console.log(v.x + ', ' + v.y);
            var dirs = ['forward', 'back', 'left', 'right'];
            var l = 0.5;
            for (i in dirs) {
                if (dirs[i] == 'forward') {
                    raycaster.ray.direction.set(this.advance.direction * v.x, 0, this.advance.direction * v.y);
                    l = 0.9;
                    }
                else if (dirs[i] == 'back') {
                    raycaster.ray.direction.set(- this.advance.direction * v.x, 0, - this.advance.direction * v.y);
                    l = 0.9;
                    }
                else if (dirs[i] == 'left') {
                    raycaster.ray.direction.set(this.advance.direction * v.y, 0, - this.advance.direction * v.x);
                    l = 0.3;
                    }
                else if (dirs[i] == 'right') {
                    raycaster.ray.direction.set(- this.advance.direction * v.y, 0, this.advance.direction * v.x);
                    l = 0.3;
                    }
                hits = raycaster.intersectObjects(DATA.COLLIDABLES);
                if (hits.length > 0) {
                    distance = hits[0].distance;
                    if (distance < l) {
                        collide = true;
                        if (dirs[i] == 'forward' || dirs[i] == 'back') {
                            var n = new THREE.Vector2(
                                hits[0].face.normal.x, 
                                hits[0].face.normal.z);
                            var a = n.angle() - v.angle();
                            a = Math.atan(Math.tan(a))
                            //console.log(dirs[i] + ', ' + a);
                            this.root.rotateY(a * delta);
                            }
                        else if (dirs[i] == 'left') {
                            //console.log(dirs[i]);
                            this.root.rotateY(-0.7 * delta);
                            }
                        else if (dirs[i] == 'right') {
                            //console.log(dirs[i]);
                            this.root.rotateY(0.7 * delta);
                            }

                        }
                    }
                }
            }

        // update of coordinates and lastPosition
        // ----------
        // mise à jour des coordonnées et de lastPosition
        INFOS.coordinates = '('
            + Math.round(this.root.position.x)
            + ' ; ' + Math.round(this.root.position.y * 10) / 10
            + ' ; ' + Math.round(this.root.position.z) + ')';
        if (collide) {
            this.root.position.copy(lastPosition);
            this.advance.speed -= 10 * delta;
            if (this.advance.speed < 0) {
                this.advance.speed = 0;
                }
            }
        };


    this.doAction = function(newAction) {
        /*
        animation management if available
        ----------
        gestion de l'animation si disponible
        */
        if (this.mixer == undefined)
            return;
        if (newAction == action)
            return;
        var actualIndex = this.animations[action];
        var newIndex = this.animations[newAction];
        if (newIndex < 0)
            newIndex = 0;
        if (actualIndex > -1)
            this.mixer.clipAction(this.animations['LIST'][actualIndex]).stop();
        this.mixer.clipAction(this.animations['LIST'][newIndex]).play();
        action = newAction;
        };


    };

