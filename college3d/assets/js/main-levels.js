/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    LEVELS MANAGEMENT
    --------------------------------------------------------------
    GESTION DES NIVEAUX
    --------------------------------------------------------------
*/


function init(event) {
    // page layout (title and list of levels)
    // ----------
    // mise en place de la page (titre et liste des niveaux)
    var text = tr('LEVELS') + ' - ' + tr(GAMES[PARAMS['what']]['name']);
    document.getElementById('GAME').textContent = text;
    text = tr('Read the explanations before clicking on a level.');
    document.getElementById('COMMENT').textContent = text;
    text = tr('Back to menu');
    document.getElementById('BACK').setAttribute('title', text);
    var levelsList = '';
    //levelsList += '<a class="list-group-item list-group-item-warning" align="center">' + tr('GAMES') + '</a>';
    for (var i=0; i < GAMES[PARAMS['what']]['levels']; i++) {
        text = tr(GAMES[PARAMS['what']][i]['name']);
        if (text != '') {
            if (i <= SCORES[PARAMS['what']])
                levelsList += '<a class="list-group-item enabled" href="#" id="' + i + '">' + text + '</a>';
            else
                levelsList += '<a class="list-group-item disabled" href="#">' + text + '</a>';
            }
        }
    document.getElementById('LEVELS').innerHTML = levelsList;


    // displaying mouse-over comments
    // ----------
    // affichage des commentaires au survol de la souris
    [].forEach.call(document.querySelectorAll('.list-group a'), function(el) {
        el.addEventListener('mouseover', function(e) {
            var id = this.getAttribute('id');
            if (id == undefined)
                document.getElementById('HELP').innerHTML = '';
            else {
                var text = tr(GAMES[PARAMS['what']][id]['comment']);
                text = tr(text).replace(/\|/g, '<br />');
                document.getElementById('HELP').innerHTML = text;
                }
            })
        });


    [].forEach.call(document.querySelectorAll('.list-group a'), function(el) {
        el.addEventListener('click', function() {
            var text = this.textContent;
            var id = this.getAttribute('id');
            if (id != undefined) {
                PARAMS['level'] = id;
                launchPage('game');
                }
            })
        });

    // return to menu with keyboard (back or space)
    // ----------
    // retour au menu avec le clavier (retour ou espace)
    document.addEventListener('keydown', function(e) {
        if ((e.which == 8) || (e.which == 32))
            launchPage('index');
        });

    };

document.getElementById('BACK').onclick = function(e) {
    launchPage('index');
    };

window.addEventListener('load', init, false);
