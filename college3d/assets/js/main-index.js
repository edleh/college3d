/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    HOME PAGE MANAGEMENT
    IN PARTICULAR THE TRANSLATIONS
    (DICTIONARY TO BE COMPLETED AT THE END OF THE MODULE)
    --------------------------------------------------------------
    GESTION DE LA PAGE D'ACCUEIL
    EN PARTICULIER LES TRADUCTIONS
    (DICTIONNAIRE À COMPLÉTER À LA FIN DU MODULE)
    --------------------------------------------------------------
*/





function init(event) {
    /*
    initialization of the page.
    If we return from the game, we manage the past parameters.
    Otherwise, the language of the browser is detected (fr by default).
    ----------
    initialisation de la page.
    Si on revient depuis le jeu, on gère les paramètres passés.
    Sinon on détecte la langue du navigateur (fr par défaut).
    */

    // update level
    // ----------
    // récupération du niveau
    if (PARAMS['level'] == undefined)
        PARAMS['level'] = 0;

    // creation of the interface
    // ----------
    // création de l'interface
    createMenus();
    if (!localStorage)
        document.getElementById('RESET').innerHTML = '';
    translateAll();

    // update check boxes
    // ----------
    // mise à jour des cases à cocher
    tools = ['sounds', 'music'];
    for (i in tools) {
        document.getElementById(tools[i]).checked = false;
        if (PARAMS[tools[i]])
            document.getElementById(tools[i]).checked = true;
        }
    if (PARAMS['infos'] == undefined)
        document.getElementById('infos').checked = true;
    else if (PARAMS['infos'] == true)
        document.getElementById('infos').checked = true;
    else
        document.getElementById('infos').checked = false;



    // reset to default setting
    // ----------
    // remettre les réglages par défaut
    document.getElementById('reset').onclick = function(e) {
        localStorage.clear();
        sessionStorage.clear();
        window.location = 'index.html';
        };

    // start the game with the keyboard (space)
    // ----------
    // lancer le jeu au clavier (espace)
    document.addEventListener('keydown', function(e) {
        if (e.which == 32)
            launchGame();
        });

    };





function createMenus() {

    function createMenu(what, title, id, list) {
        //console.log(title + '|' + id + '|' + list);
        var menu = '';
        menu += '<label class="input-group-text tr-text" for="inputGroupSelect' + what + '">' + title + '</label>';
        if (what == 'lang')
            menu += '<select class="form-select" id="inputGroupSelect' + what + '" onchange="langChanged()">';
        else if (what == 'control')
            menu += '<select class="form-select" id="inputGroupSelect' + what + '" onchange="updateHelpPanel()">';
        else
            menu += '<select class="form-select" id="inputGroupSelect' + what + '">';
        for (i in list) {
            if (list[i] == 'SEPARATOR')
                menu +=     '<option style="font-size: 50%;" disabled>──────────────────────────────</option>';
            else if (list[i] == id)
                menu +=     '<option selected value="' + list[i] + '">' + translate(list[i]) + '</option>';
            else
                menu +=     '<option value="' + list[i] + '">' + translate(list[i]) + '</option>';
            }
        menu += '</select>';
        return menu;
        };

    var list = [];
    var menu = '';

    if (PARAMS['what'] == undefined)
        PARAMS['what'] = 'free';
    //list = ['free', 'SEPARATOR', 'discover', 'seasons', '2003-2013', 'SEPARATOR', 'exhibition', 'SEPARATOR', 'lan'];
    //list = ['free', 'SEPARATOR', 'discover', 'SEPARATOR', 'exhibition'];
    list = ['free', 'discover', 'SEPARATOR', 'exhibition'];
    menu = createMenu('what', 'GAME', PARAMS['what'], list);
    document.getElementById('what').innerHTML = menu;

    if (PARAMS['character'] == undefined)
        PARAMS['character'] = 'nothing';
    //list = ['nothing', 'student', 'teacher', 'dog', 'moupton', 'pingus', 'Nono_Baker', 'robot', 'tour', 'kiwi', 'kangaroo'];
    list = ['nothing', 'student', 'teacher', 'dog', 'moupton', 'pingus', 'Nono_Baker', 'robot', 'tour', 'kiwi', ];
    menu = createMenu('character', 'CHARACTER', PARAMS['character'], list);
    document.getElementById('character').innerHTML = menu;

    if (PARAMS['control'] == undefined)
        PARAMS['control'] = 'keyboard';
    list = ['keyboard', 'mouse', 'touch', 'gamepad'];
    menu = createMenu('control', 'CONTROLLER', PARAMS['control'], list);
    document.getElementById('control').innerHTML = menu;

    if (PARAMS['effect'] == undefined)
        PARAMS['effect'] = 'nothing';
    list = ['nothing', 'anaglyph', 'stereo', 'ascii', 'outline'];
    menu = createMenu('effect', 'EFFECT', PARAMS['effect'], list);
    document.getElementById('effect').innerHTML = menu;
    menu = createMenu('lang', 'LANG', PARAMS['lang'], LANGUAGES);
    document.getElementById('lang').innerHTML = menu;
    }

function langChanged() {
    var e = document.getElementById('inputGroupSelectlang');
    var strUser = e.options[e.selectedIndex].value;
    PARAMS['lang'] = strUser;
    saveData();
    launchPage('index');
    }

function saveData() {
    /*
    save the parameters
    ----------
    enregistre les paramètres
    */
    tools = ['what', 'character', 'control', 'effect', 'lang'];
    for (i in tools) {
        var e = document.getElementById('inputGroupSelect' + tools[i]);
        var strUser = e.options[e.selectedIndex].value;
        //console.log(tools[i] + '|' + strUser);
        PARAMS[tools[i]] = strUser;
        }
    tools = ['sounds', 'music', 'infos'];
    for (i in tools)
        PARAMS[tools[i]] = document.getElementById(tools[i]).checked;
    }

function launchGame() {
    /*
    starts the game with the parameters
    ----------
    lance le jeu en indiquant les paramètres
    */
    saveData();
    if (PARAMS['what'] == 'free')
        launchPage('game');
    else
        launchPage('levels');
    }

document.getElementById('GO').onclick = function(e) {
    launchGame();
    };

function updateHelpPanel() {
    /*
    update of the explanation panel according to the selected controller
    ----------
    mise à jour du panneau d'explications selon le contrôleur sélectionné
    */
    var text = '';
    var e = document.getElementById('inputGroupSelectcontrol');
    var control = e.options[e.selectedIndex].value;

    if (control == 'keyboard')
        text = translate('movements: arrows, WASD ----- jumps: space bar')
            + translate('|camera change: C')
            + translate('|exit: Esc ----- pause: P ----- review last message: M');
    else if (control == 'mouse')
        text = translate('movements: left-right, left bottom ----- jumps: right click')
            + translate('|camera change: scroll wheel, C')
            + translate('|exit: Esc ----- pause: P ----- review last message: M');
    else if (control == 'touch')
            text = translate('movements: ttt ----- jumps: ttt')
            + translate('|camera change: ttt, C')
            + translate('|exit: Esc ----- pause: P ----- review last message: M');
    else if (control == 'gamepad')    
            text = translate('movements: axes ----- jumps: button 1')
            + translate('|camera change: button 2, C')
            + translate('|exit: button 5, Esc ----- pause: button 6, P ----- review last message: button 3, M');
    text = text.replace(/\|/g, '<br />');
    document.getElementById('HELP').innerHTML = text;
    }





function translateAll() {
    /*
    update of the entire interface depending on the language requested.
    ----------
    mise à jour de toute l'interface en fonction de la langue demandée.
    */
    var text = '';

    [].forEach.call(document.querySelectorAll('.tr-text'), function(el) {
        text = el.textContent;
        el.textContent = translate(text);
        });

    [].forEach.call(document.querySelectorAll('.tr-title'), function(el) {
        text = el.getAttribute('title');
        el.setAttribute('title', translate(text));
        });

    [].forEach.call(document.querySelectorAll('.dropdown-menu li a'), function(el) {
        text = el.getAttribute('id');
        el.textContent = translate(text);
        });

    [].forEach.call(document.querySelectorAll('.dropdown-toggle'), function(el) {
        text = el.getAttribute('id');
        el.innerHTML = translate(text) + ' <span class="caret"></span>';
        });

    updateHelpPanel();
    }

function translate(text) {
    /*
    returns the translated text
    ----------
    retourne le texte traduit
    */
    var result = CONNECTIONS[text.trim()];
    if (result === undefined)
        result = tr(text);
    else
        result = tr(result);
    return result;
    }

var CONNECTIONS = {
    'free': 'Free visit', 
    'discover': 'Discover the college', 
    'seasons': 'During seasons', 
    '2003-2013': '2003-2013', 
    'exhibition': 'The exhibition', 
    'lan': 'LAN play', 

    'touch': 'touchscreen', 

    'fr': 'Français', 
    'en': 'English', 
    'de': 'Deutsch', 
    'es': 'Español', 
    'lt': 'Latina', 
    'lol': 'LOL', 
    };


window.addEventListener('load', init, false);
