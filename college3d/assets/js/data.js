/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    GAME DATA
    --------------------------------------------------------------
    DONNÉES DU JEU
    --------------------------------------------------------------
*/





Data = function() {

    this.enabled = false;

    this.LOADED = 0;
    this.NB_MESHS = 0;
    this.NB_LOADEDS = 0;

    // ground (terrain etc):
    // ----------
    // sol (terrain etc) :
    this.GROUND = {
        'objects': ['terrain/terrain', ], 
        'collidables': [], 
        };

    // always displayed (loaded at launch):
    // ----------
    // toujours affichés (chargés au lancement) :
    this.ALWAYS = [
        'entree/sapin', 'entree/abris-velos', 'entree/portail', 
        'tour/tour', 
        'hall/hall', 
        'passerelles/passerelles', 
        'batiments/A/a', 
        'batiments/batiments', 
        ];

    // the details 
    // (displayed if one is close enough, hence the coordinates and display distance):
    // ----------
    // les détails 
    // (affichés si on est assez près, d'où les coordonnées et la distance d'affichage) :
    this.DETAILS = [
        // x, z, d, visible, laoded, mesh, files
        [-31, -12, 40, false, false, undefined, [
            'batiments/A/a0-salles', 
            'batiments/A/a1-salles', 
            'batiments/A/escaliers', 
            ]], 
        [0, -28, 30, false, false, undefined, ['entree/entree-details', ]], 
        [13, -2, 30, false, false, undefined, ['hall/hall-details', ]], 
        [14, 24, 30, false, false, undefined, ['tour/tour-details', ]], 
        ];

    // displayed only for some games:
    // ----------
    // affichés seulement pour certains jeux :
    this.GAMES_OBJECTS = {
        'exhibition': ['expo/expo', ], 
        };

    // list of collidables objects:
    // ----------
    // liste des objets à prendre en compte pour les collisions :
    this.COLLIDABLES = [];

    };
