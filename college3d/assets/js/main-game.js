/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    PLACEMENT AND MANAGEMENT OF THE GAME
    --------------------------------------------------------------
    MISE EN PLACE ET GESTION DU JEU
    --------------------------------------------------------------
*/





// THREEJS VARIABLES
// ----------
// VARIABLE POUR THREEJS
var scene, camera, renderer, container, controls;
var loader_GLTF = new THREE.GLTFLoader();

var clock = new THREE.Clock();
var TIMER = 0;
var CHRONO = {'0.1': 0, '1': 0, '2': 0, '5': 0, };
var WITH_EFFECT;

// GAME VARIABLES
// ----------
// VARIABLES DU JEU
var INFOS = {}, MESSAGE_DIV, MODAL_DIV, MODAL_CLOSE;
var DATA, CHARACTER, GAME;

// SCREEN VARIABLES
// ----------
// VARIABLES DE L'ÉCRAN
var HEIGHT, WIDTH, TOP = 52;





function createScene() {
    HEIGHT = (window.innerHeight - TOP);
    WIDTH = window.innerWidth;

    camera = new THREE.PerspectiveCamera(60, WIDTH / HEIGHT, 1, 1000);

    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer({antialias: false, alpha: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(WIDTH, HEIGHT);

    // setting up the interface
    // ----------
    // mise en place de l'interface
    document.getElementById('Esc').innerHTML = tr('Esc');
    document.getElementById('Esc').setAttribute('title', tr('exit'));
    document.getElementById('P').setAttribute('title', tr('pause'));
    document.getElementById('M').setAttribute('title', tr('review last message'));
    document.getElementById('C').setAttribute('title', tr('change camera'));

    INFOS.centerdiv = document.getElementById('infos-center');
    INFOS.leftdiv = document.getElementById('infos-left');
    INFOS.rightdiv = document.getElementById('infos-right');
    INFOS.info = tr('LOADING...');
    //console.log('LOADING...');
    INFOS.coordinates = '-';
    INFOS.fps = '-';
    MESSAGE_DIV = document.getElementById('message');
    MODAL_DIV = document.getElementById('myModal');
    MODAL_CLOSE = document.getElementsByClassName("modal-close")[0];

    container = document.getElementById('container-game');
    var control = PARAMS['control'];
    if (CONTROLS.indexOf(control) < 0)
        control = 'keyboard';
    if (control == 'keyboard' || control == 'gamepad')
        container.style.cursor = 'none';
    INFOS.other = '<p>' + tr(control) + '</p><p>' + tr(PARAMS['character']) + '</p>';

    // management of the chosen effect
    // ----------
    // gestion de l'effet choisi
    var effect_name = PARAMS['effect'];
    if (EFFECTS.indexOf(effect_name) < 0)
        WITH_EFFECT = false;
    else {
        WITH_EFFECT = true;
        if (effect_name == 'anaglyph')
            effect = new THREE.AnaglyphEffect(renderer);
        else if (effect_name == 'stereo')
            effect = new THREE.StereoEffect(renderer);
        else if (effect_name == 'ascii')
            effect = new THREE.AsciiEffect(renderer);
        else if (effect_name == 'outline')
            effect = new THREE.OutlineEffect(renderer);
        effect.setSize(WIDTH, HEIGHT);
        }
    if (effect_name == 'ascii') {
        container.appendChild(effect.domElement);
        controls = new THREE.InputControls(effect.domElement, control);
        }
    else {
        container.appendChild(renderer.domElement);
        controls = new THREE.InputControls(renderer.domElement, control);
        }

    window.addEventListener('resize', onWindowResize, false);
    // When the user clicks on <span> (x), close the modal
    MODAL_CLOSE.onclick = function() {
        if (MODAL_CLOSE == undefined)
            return;
        hideModal();
        };
    }

function createWeather() {
    // lighting and weather
    // ----------
    // éclairage et météo
    var light = new THREE.HemisphereLight(0xffffbb, 0x080820, 0.5);
    scene.add(light);
    //var light = new THREE.AmbientLight(0x404040);
    var light = new THREE.AmbientLight(0xdddddd);
    scene.add(light);
    }

function createData() {
    // loading objects of scenery
    // ----------
    // chargement des objets du décor
    DATA = new Data();
    for (i in DATA.GROUND['objects']) {
        DATA.NB_MESHS += 1;
        loader_GLTF.load(
            'assets/data/college/' + DATA.GROUND['objects'][i] + '.glb', 
            function(gltf) {
                gltf.scene.traverse(
                    function(child) {
                        if (child.isMesh)
                            DATA.GROUND.collidables.push(child);
                        }
                    );
                scene.add(gltf.scene);
                DATA.NB_LOADEDS += 1;
                }
            );
        }
    for (i in DATA.ALWAYS) {
        DATA.NB_MESHS += 1;
        loader_GLTF.load(
            'assets/data/college/' + DATA.ALWAYS[i] + '.glb', 
            function(gltf) {
                gltf.scene.traverse(
                    function(child) {
                        if (child.isMesh) {
                            DATA.COLLIDABLES.push(child);
                            DATA.GROUND.collidables.push(child);
                            }
                        }
                    );
                scene.add(gltf.scene);
                DATA.NB_LOADEDS += 1;
                }
            );
        }

    // the game object
    // ----------
    // l'objet jeu
    GAME = new Game();
    var initialPosition = [10, 20, -40];
    if (PARAMS['what'] != 'free') {
        initialPosition = GAME.init(PARAMS['level']);
        loader_GLTF.load(
            'assets/data/' + GAME.mission.target[2] + '.glb', 
            function(gltf) {
                GAME.target = gltf.scene;
                GAME.target.position.set(
                    GAME.targetPosition[0], 
                    GAME.targetPosition[1], 
                    GAME.targetPosition[2]);
                scene.add(GAME.target);
                }
            );
        }
    if (DATA.GAMES_OBJECTS[PARAMS['what']] != undefined)
        for (i in DATA.GAMES_OBJECTS[PARAMS['what']]) {
            loader_GLTF.load(
                'assets/data/' + DATA.GAMES_OBJECTS[PARAMS['what']][i] + '.glb', 
                function(gltf) {
                    gltf.scene.traverse(
                        function(child) {
                            if (child.isMesh) {
                                DATA.COLLIDABLES.push(child);
                                DATA.GROUND.collidables.push(child);
                                }
                            }
                        );
                    scene.add(gltf.scene);
                    }
                );
            }

    // the character
    // ----------
    // le personnage
    CHARACTER = new THREE.Character();
    CHARACTER.init({
        'position': initialPosition, 
        'rotationY': 3.14, 
        });
    if (PARAMS['character'] != 'nothing') {
        DATA.NB_MESHS += 1;
        loader_GLTF.load(
            'assets/data/chars/' + PARAMS['character'] + '/' + PARAMS['character'] + '.glb', 
            function(gltf) {
                CHARACTER.mesh = gltf.scene;
                CHARACTER.mesh.position.set(0, -1, 0);
                CHARACTER.mesh.rotateY(3.14);

                CHARACTER.mixer = new THREE.AnimationMixer(CHARACTER.mesh);
                CHARACTER.animations['LIST'] = gltf.animations;
                for (var i=0; i < gltf.animations.length; i++)
                    CHARACTER.animations[gltf.animations[i].name] = i;
                DATA.NB_LOADEDS += 1;
                //console.log(gltf.animations);
                //console.log(CHARACTER.animations['LIST']);
                }
            );
        }
    else {
        var geometry = new THREE.BoxGeometry(0.5, 1.7, 0.5);
        var material = new THREE.MeshLambertMaterial(
            {color: 0x8888ff, shading: THREE.FlatShading});
        CHARACTER.mesh = new THREE.Mesh(geometry, material);
        CHARACTER.mesh.position.set(0, -0.3, 0);
        }
    }

function loop() {
    requestAnimationFrame(loop);

    // we wait until all the decors are loaded
    // ----------
    // on attend que tous les décors soient chargés
    if (DATA.LOADED < 1) {
        //console.log(DATA.NB_MESHS + ' ' + DATA.NB_LOADEDS);
        updateInfos();
        if (DATA.NB_LOADEDS > DATA.NB_MESHS - 1)
            DATA.LOADED = 1;
        return;
        }

    var delta = clock.getDelta();
    updateTimer(delta);    

    // the game can start
    // ----------
    // le jeu peut commencer
    if (DATA.LOADED < 9) {
        if (TIMER > 1) {
            //console.log('LOADED !!!');
            INFOS.info = tr('LOADED !!!');
            controls.enabled = true;
            CHARACTER.enabled = true;
            DATA.LOADED = 9;
            if (PARAMS['what'] == 'free')
                showMessage('Free visit');
            else {
                GAME.state = 'ENABLED';
                showMessage(
                    GAMES[PARAMS['what']][PARAMS['level']]['name'], 
                    parseInt(PARAMS['level']) + 1, 
                    GAME.mission.text);
                }
            }
        return;
        }

    CHARACTER.update(delta);
    controls.update();

    if (WITH_EFFECT)
        effect.render(scene, camera);
    else
        renderer.render(scene, camera);
    }

function updateTimer(delta) {
    // does not update every display
    // ----------
    // permet de ne pas mettre tout à jour à chaque affichage
    TIMER += delta;
    if (TIMER - CHRONO['5'] > 5) {
        CHRONO['5'] = CHRONO['2'] = CHRONO['1'] = CHRONO['0.1'] = TIMER;
        INFOS.info = '';
        MESSAGE_DIV.style.display = 'none';
        INFOS.fps = Math.round(1 / delta);
        if (GAME.state == 'ENABLED' || GAME.state == 'LOST')
            GAME.update(true);
        updateInfos();
        updateDetails();
        }
    else if (TIMER - CHRONO['2'] > 2) {
        CHRONO['2'] = CHRONO['1'] = CHRONO['0.1'] = TIMER;
        INFOS.fps = Math.round(1 / delta);
        if (GAME.state == 'ENABLED' || GAME.state == 'LOST')
            GAME.update(true);
        updateInfos();
        updateDetails();
        }
    else if (TIMER - CHRONO['1'] > 1) {
        CHRONO['1'] = CHRONO['0.1'] = TIMER;
        INFOS.fps = Math.round(1 / delta);
        if (GAME.state == 'ENABLED' || GAME.state == 'LOST')
            GAME.update(true);
        updateInfos();
        updateDetails();
        }
    else if (TIMER - CHRONO['0.1'] > 0.1) {
        CHRONO['0.1'] = TIMER;
        if (PARAMS['infos'])
            updateInfos();
        if (GAME.state == 'ENABLED')
            GAME.update();
        }
    }

function updateInfos() {
    // display of game infos
    // ----------
    // affichage des infos du jeu
    if (PARAMS['infos']) {
        //INFOS.coordinates = CHARACTER.advance.speed;
        INFOS.leftdiv.innerHTML = '<p></p><p>' + INFOS.coordinates + '</p><p>' + INFOS.fps + ' FPS</p>';
        if (GAME.state == 'ENABLED' && GAME.timer[0] > 0)
            INFOS.centerdiv.innerHTML = '<b>' + GAME.timer[0] + '</b>';
        else if (INFOS.info != '')
            INFOS.centerdiv.innerHTML = '<b>' + INFOS.info + '</b>';
        else
            INFOS.centerdiv.innerHTML = '';
        INFOS.rightdiv.innerHTML = INFOS.other;
        }
    else
        INFOS.centerdiv.innerHTML = '<b>' + INFOS.info + '</b>';
    }

function showMessage(message, level, mission) {
    // display of game messages
    // ----------
    // affichage des messages du jeu
    if (message != undefined) {
        var html = '';
        if (level != undefined)
            html += tr('Level') + ' ' + level + ' - ';
        html += tr(message).replace(/\|/g, '<br />');
        MESSAGE_DIV.innerHTML = '<h4><b>' + html + '</b></h4>';
        if (mission != undefined) {
            html = tr(mission).replace(/\|/g, '<br />');
            MESSAGE_DIV.innerHTML += '<h3><b>' + html + '</b></h3>';
            }
        }
    CHRONO['5'] = TIMER;
    MESSAGE_DIV.style.display = 'block';
    }

function updateDetails() {
    // the details are displayed according to the distance
    // ----------
    // les détails sont affichés en fonction de la distance
    if (DATA.LOADED < 10)
        return;
    var x = CHARACTER.root.position.x;
    var z = CHARACTER.root.position.z;
    var xx = CHARACTER.oldPosition.x;
    var zz = CHARACTER.oldPosition.z;
    var d = Math.sqrt((x - xx)*(x - xx) + (z - zz)*(z - zz));
    if (d > 2) {
        //console.log('updateDetails: ' + CHARACTER.oldPosition.x + ',' + CHARACTER.oldPosition.z + ' --- ' + d)
        CHARACTER.oldPosition.x = x;
        CHARACTER.oldPosition.z = z;
        for (i in DATA.DETAILS) {
            xx = DATA.DETAILS[i][0];
            zz = DATA.DETAILS[i][1];
            d = Math.sqrt((x - xx)*(x - xx) + (z - zz)*(z - zz));
            if (d < DATA.DETAILS[i][2] && DATA.DETAILS[i][4] == false) {
                DATA.DETAILS[i][4] = true;
                if (DATA.DETAILS[i][3] == false) {
                    var group = new THREE.Group();
                    DATA.DETAILS[i][5] = group;
                    scene.add(group);
                    for (j in DATA.DETAILS[i][6]) {
                        var name = DATA.DETAILS[i][6][j];
                        var i2 = i;
                        //console.log(i2 + ', ' + name);
                        loader_GLTF.load(
                            'assets/data/college/' + name + '.glb', 
                            function(gltf) {
                                gltf.scene.traverse(
                                    function(child) {
                                        if (child.isMesh) {
                                            DATA.COLLIDABLES.push(child);
                                            DATA.GROUND.collidables.push(child);
                                            }
                                        }
                                    );
                                DATA.DETAILS[i2][3] = true;
                                //console.log(i2 + ' : ' + d + ' => LOAD');
                                group.add(gltf.scene);
                                }
                            );
                        }
                    }
                else {
                    scene.add(DATA.DETAILS[i][5]);
                    //console.log(i + ' : ' + d + ' => add');
                    }
                }
            else if (d > DATA.DETAILS[i][2] && DATA.DETAILS[i][4] == true) {
                DATA.DETAILS[i][4] = false;
                scene.remove(DATA.DETAILS[i][5]);
                //console.log(i + ' : ' + d + ' => remove');
                }
            }
        }
    }

function showModal(what) {
    // display of game messages
    // ----------
    // affichage des messages du jeu
    if (what == 'PAUSE') {
        var myModalText = document.getElementById('myModalText');
        myModalText.innerHTML = tr('PAUSE');
        controls.enabled = false;
        CHARACTER.enabled = false;
        MODAL_DIV.style.display = 'block';
        }
    }

function hideModal() {
    // display of game messages
    // ----------
    // affichage des messages du jeu
    controls.enabled = true;
    CHARACTER.enabled = true;
    MODAL_DIV.style.display = "none";
    }




function onWindowResize() {
    // the display is adapted to the dimensions of the window
    // ----------
    // on adapte l'affichage aux dimensions de la fenêtre
    HEIGHT = (window.innerHeight - TOP);
    WIDTH = window.innerWidth;
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
    if (WITH_EFFECT)
        effect.setSize(WIDTH, HEIGHT);
    else
        renderer.setSize(WIDTH, HEIGHT);
    controls.handleResize();
    }

function init(event) {
    createScene();
    createWeather();
    createData();
    loop();
    }

window.addEventListener('load', init, false);

