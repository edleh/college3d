/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    SCRIPT LOADING SYSTEM NEEDED
    BASED ON FILE PARAMETERS
    --------------------------------------------------------------
    SYSTÈME DE CHARGEMENT DES SCRIPTS NÉCESSAIRES
    EN FONCTION DES PARAMÈTRES PASSÉS AU FICHIER
    --------------------------------------------------------------
*/





// GLOBAL VARIABLES (USED IN THE GAME)
// ----------
// VARIABLES GLOBALES (UTILISÉES DANS LE JEU)

//var LANGUAGES = ['fr', 'en', 'de', 'es', 'lt', 'SEPARATOR', 'lol'];
var LANGUAGES = ['fr', 'en', 'SEPARATOR', 'lol'];

var CONTROLS = ['keyboard', 'mouse', 'touch', 'gamepad', ];
var EFFECTS = ['anaglyph', 'stereo', 'ascii', 'outline', ];

var PARAMS = {};
var SCORES = {
    'free': 0, 
    'discover': 0, 
    'seasons': 0, 
    '2003-2013': 0, 
    'exhibition': 0, 
    'lan': 0, 
    };





// USEFUL FUNCTIONS
// ----------
// FONCTIONS UTILES
function extractHtmlFileName() {
    /*
    http://befused.com/javascript/get-filename-url
    */
    var url = window.location.pathname;
    var fileName = url.substring(url.lastIndexOf('/')+1);
    fileName = fileName.split('.')[0];
    return fileName;
    }

function launchPage(who) {
    /*
    launch the page with the parameters
    ----------
    lance la page en indiquant les paramètres
    */
    var page = who + '.html';
    if (localStorage) {
        var json = JSON.stringify(PARAMS);
        localStorage.setItem('C3D', json);
        json = JSON.stringify(SCORES);
        sessionStorage.setItem('C3D', json);
        }
    else {
        page += '?what=' + PARAMS['what'];
        page += '&control=' + PARAMS['control'];
        page += '&effect=' + PARAMS['effect'];
        page += '&character=' + PARAMS['character'];
        page += '&lang=' +  PARAMS['lang'];
        page += '&level=' + PARAMS['level'];
        if (PARAMS['infos'])
            page += '&infos=true';
        else
            page += '&infos=false';
        if (PARAMS['sounds'])
            page += '&sounds=true';
        if (PARAMS['music'])
            page += '&music=true';
        page = page.replace(/\s/g, '');
        }
    window.location = page;
    }





// MAIN FUNCTION
// ----------
// FONCTION PRINCIPALE
function main() {
    /*
    we retrieve the parameters, fill in the list INCLUDES
    and run the include function
    ----------
    on récupère les paramètres, on rempli la liste INCLUDES
    et on lance la fonction include
    */

    function include(urls, callback) {
        /*
        http://sametmax.com/include-require-import-en-javascript
        */

        for (i in urls) {
            var url = urls[i];
            /* on crée une balise<script type="text/javascript"></script> */
            var script = document.createElement('script');
            script.type = 'text/javascript';

            /* On fait pointer la balise sur le script qu'on veut charger
            avec en prime un timestamp pour éviter les problèmes de cache
            */
            script.src = url + '?' + (new Date().getTime());

            /* On dit d'exécuter cette fonction une fois que le dernier script est chargé */
            if (i == urls.length - 1)
                if (callback) {
                    script.onreadystatechange = callback;
                    script.onload = script.onreadystatechange;
                    }

            /* On rajoute la balise script dans le head, ce qui démarre le téléchargement */
            document.getElementsByTagName('head')[0].appendChild(script);
            }
        };

    var FILE_NAME = extractHtmlFileName();
    if (FILE_NAME == '' || FILE_NAME == undefined)
        FILE_NAME = 'index';
    var INCLUDES = [];

    if (localStorage) {
        var json = localStorage.getItem('C3D');
        if (json != null)
            PARAMS = JSON.parse(json);
        json = sessionStorage.getItem('C3D');
        if (json != null)
            SCORES = JSON.parse(json);
        }
    else {
        // retrieving page settings
        // ----------
        // récupération des paramètres de la page
        // http://www.helary.net/recuperer-appel-parametres-javascript
        var t = window.location.search.substring(1).split('&');
        for (i in t) {
            var x = t[i].split('=');
            PARAMS[x[0]] = x[1];
            }
        }
    //console.log(PARAMS);
    //console.log(SCORES);

    // we start by finding the language of the interface
    // ----------
    // on commence par trouver la langue de l'interface
    if (PARAMS['lang'] == undefined)
        PARAMS['lang'] = navigator.language;
    if (LANGUAGES.indexOf(PARAMS['lang']) < 0)
        PARAMS['lang'] = 'fr';

    if (FILE_NAME == 'game') {
        INCLUDES.push('assets/js/data.js');
        INCLUDES.push('assets/js/scenarios.js');
        INCLUDES.push('assets/js/translations/' + PARAMS['lang'] + '.js');
        INCLUDES.push('assets/js/controls/InputControls.js');
        INCLUDES.push('assets/js/controls/Character.js');
        INCLUDES.push('assets/libs/loaders/GLTFLoader.js');
        var effect = PARAMS['effect'];
        if (EFFECTS.indexOf(effect) > -1) {
            var effectFile = '';
            if (effect == 'anaglyph')
                effectFile = 'AnaglyphEffect';
            else if (effect == 'stereo')
                effectFile = 'StereoEffect';
            else if (effect == 'ascii')
                effectFile = 'AsciiEffect';
            else if (effect == 'outline')
                effectFile = 'OutlineEffect';
            INCLUDES.push('assets/libs/effects/' + effectFile + '.js');
            }
        }
    else if (FILE_NAME == 'levels') {
        INCLUDES.push('assets/js/scenarios.js');
        INCLUDES.push('assets/js/translations/' + PARAMS['lang'] + '.js');
        }
    else if (FILE_NAME == 'index') {
        INCLUDES.push('assets/js/translations/' + PARAMS['lang'] + '.js');
        }
    else if (FILE_NAME == 'indexEX') {
        INCLUDES.push('assets/js/translations/' + PARAMS['lang'] + '.js');
        }

    if (INCLUDES.length < 1)
        include(['assets/js/main-' + FILE_NAME + '.js']);
    else {
        include(
            INCLUDES, 
            function() {
                include(['assets/js/main-' + FILE_NAME + '.js']);
                }
            )
        }
    }

main();

