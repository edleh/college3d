/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


var TRANSLATIONS = {
    'GAME': 'JEU xd', 
    'CHARACTER': 'PERSONAGE', 

    'CONTROLLER': 'KONTROLEUR', 
    'movements: arrows, WASD ----- jumps: space bar': 
        'déplassmen : flèchs, WASD ----- sots : bar éspass', 
    '|camera change: C': '|changmen de kaméra : C', 
    '|exit: Esc ----- pause: P ----- review last message: M': 
        '|kité : Échap ----- poz : P ----- revoir le dernié méssaj : M', 
    'movements: left-right, left bottom ----- jumps: right click': 
        'déplassmen : gauch-drouat, bouton goch ----- sots : klic droua', 
    '|camera change: scroll wheel, C': '|changmen de kaméra : molett, C', 
    'movements: ttt ----- jumps: ttt': 'déplassmen : ttt ----- sots : ttt', 
    '|camera change: ttt, C': '|changmen de kaméra : ttt, C', 
    'movements: axes ----- jumps: button 1': 'déplassmen : ax ----- sots : bouton 1', 
    '|camera change: button 2, C': '|changmen de kaméra : bouton 2, C', 
    '|exit: button 5, Esc ----- pause: button 6, P ----- review last message: button 3, M': 
        '|kité : bouton 5, Échap ----- poz : bouton 6, P ----- revoir le dernié méssaj : bouton 3, M', 

    'EFFECT': 'ÉFÉ', 
    'anaglyph': 'anaglif', 
    'stereo': 'stayréo', 
    'ascii': 'aski', 
    'outline': 'kontour', 

    'LANG': 'LENG mdr', 

    'SOUNDS': 'SONTS', 
    'MUSIC': 'MUZIK', 
    'INFOS': 'INFO XD', 

    'Help of the game': 'Aid du jeux', 
    'Project web page': 'Page waib du progé', 
    'Reset to default settings': 'Remettr lé réglaj par défo', 
    'PLAY': 'JOUÉ.xd', 


    'Esc': 'Échap', 
    'exit': 'kité', 
    'pause': 'poz', 
    'review last message': 'revoir le dernié méssaj', 
    'change camera': 'changé de kaméra', 

    'LOADING...': 'CHARGMEN xD...', 
    'LOADED !!!': 'CHARGÉ ptdr !!!', 
    'QUIT THE GAME ?': 'KITÉ LE JEU ???', 

    'keyboard': 'klavié', 
    'mouse': 'soury', 
    'touchscreen': 'ékran taktil', 
    'gamepad': 'manett', 

    'nothing': 'okun', 
    'student': 'élèv', 
    'teacher': 'prof mdr', 
    'dog': 'chyn', 
    'tower': 'toure', 
    'robot': 'robo', 
    'kangaroo': 'kangrou', 

    'LEVELS': 'NIVOS', 
    'Read the explanations before clicking on a level.': 
        'Li bien lez esplikation avan de cliqué sur un nivo.', 
    'Back to menu': 'Retour o menu', 
    'Level': 'Nivo', 
    'YOU FINISHED THIS GAME!': 'TU A TERMINER SE JEU.xd !', 
    'YOU LOST!': 'TU A PERDU ! mdr !!!', 


    'Free visit': 'Vizit libr', 


    'Discover the college': 'Dékouvrir le colg', 
    'Before Enter': "Avan d'entré", 
    "You'll find different places to college entrance.||Initially a marker will help you.": 
        "Tu devra trouvais diférant lieuh a lentrer du colaije.||Ô daipar in reupére tèderas.", 
    'Go to the gate.': "Va jusko portail.", 
    'Go under the bike garage.': 'Va sou le garaje a vélo.', 
    'Go to the front door.': "Va juska la porte d'entré.", 
    'The Hall': 'Sou le Hall', 
    'Everything happens this time under the hall.': 'Tou ce paçe cete foi sou leu olle', 
    'Go between the 2 fountains.': 'Va entre lé 2 fontaine.', 
    'Go in front of the reception.': "Va devan l'akueil.", 
    'Go to the exit of the refectory.': 'Va a la sorti du réfectoir.', 
    'Near the tower': 'Pré de la tour', 
    'Because there is a tower in the school yard.': 'Kar île ia 1 tourre dent la courre du quollaije', 
    'Go between the ping-pong tables.': 'Va entre lé tables de ping-pong.', 
    'Go to the tower.': 'Va a la tour.', 
    'Go to the entrance of the cafeteria.': "Va a l'entré de la kantine.", 
    'The buildings': 'Lé batimens', 
    'You will know the building entrances.': 'Tu connètras les antrais dé batimants.', 
    'Go under the small hall.|(building A)': 'Va sous le peti holl.|(batimen A)', 
    'Go to the stairs of building A.': 'Va devan lezs escalier du batimen A.', 
    'Go to the entrance of Building B.': "Va a l'entré du batimen B.", 
    'Go to the entrance of the CDI.|(building C)': "Va a l'entré du CDI.|(batimen C)", 
    'Go to the entrance of the technology rooms.|(building C)': 
        "Va a l'entrer des sall de teknologi.|(batimen C)", 


    'During seasons': 'O cour dé sézon', 


    '2003-2013': '', 


    'The exhibition': "L'éxpo", 
    'Explanations': 'Explikassions', 
    "You still did not know what's inside the tower.||A secret building is hidden there.": 
        "Tu ne savait pa encor ce qui y a a l'intérieur de la toure.||Un batiment secré y et cacher.", 
    'The exhibition takes place in the tower.': "L'exposition a lieu dan la toure.", 


    'LAN play': 'Joué en rézo', 


    '': '', 
    '': '', 
    '': '', 
    };





function tr(text) {
    var result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }


