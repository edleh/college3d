/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


var TRANSLATIONS = {
    'GAME': '', 
    'CHARACTER': '', 

    'CONTROLLER': '', 
    'movements: arrows, WASD ----- jumps: space bar': '', 
    '|camera change: C': '', 
    '|exit: Esc ----- pause: P ----- review last message: M': '', 
    'movements: left-right, left bottom ----- jumps: right click': '', 
    '|camera change: scroll wheel, C': '', 
    'movements: ttt ----- jumps: ttt': '', 
    '|camera change: ttt, C': '', 
    'movements: axes ----- jumps: button 1': '', 
    '|camera change: button 2, C': '', 
    '|exit: button 5, Esc ----- pause: button 6, P ----- review last message: button 3, M': '', 

    'EFFECT': '', 
    'anaglyph': '', 
    'stereo': '', 
    'ascii': '', 
    'outline': '', 

    'LANG': '', 

    'SOUNDS': '', 
    'MUSIC': '', 
    'INFOS': '', 

    'Help of the game': '', 
    'Project web page': '', 
    'Reset to default settings': '', 
    'PLAY': '', 


    'Esc': '', 
    'exit': '', 
    'pause': '', 
    'review last message': '', 
    'change camera': '', 

    'LOADING...': '', 
    'LOADED !!!': '', 
    'QUIT THE GAME ?': '', 

    'keyboard': '', 
    'mouse': '', 
    'touchscreen': '', 
    'gamepad': '', 

    'nothing': '', 
    'student': '', 
    'teacher': '', 
    'dog': '', 
    'tower': '', 
    'robot': '', 
    'kangaroo': '', 

    'LEVELS': '', 
    'Read the explanations before clicking on a level.': '', 
    'Back to menu': '', 
    'Level': '', 
    'YOU FINISHED THIS GAME!': '', 
    'YOU LOST!': '', 


    'Free visit': '', 


    'Discover the college': '', 
    'Before Enter': '', 
    "You'll find different places to college entrance.||Initially a marker will help you.": '', 
    'Go to the gate.': '', 
    'Go under the bike garage.': '', 
    'Go to the front door.': '', 
    'The Hall': '', 
    'Everything happens this time under the hall.': '', 
    'Go between the 2 fountains.': '', 
    'Go in front of the reception.': '', 
    'Go to the exit of the refectory.': '', 
    'Near the tower': '', 
    'Because there is a tower in the school yard.': '', 
    'Go between the ping-pong tables.': '', 
    'Go to the tower.': '', 
    'Go to the entrance of the cafeteria.': '', 
    'The buildings': '', 
    'You will know the building entrances.': '', 
    'Go under the small hall.|(building A)': '', 
    'Go to the stairs of building A.': '', 
    'Go to the entrance of Building B.': '', 
    'Go to the entrance of the CDI.|(building C)': '', 
    'Go to the entrance of the technology rooms.|(building C)': '', 


    'During seasons': '', 


    '2003-2013': '', 


    'The exhibition': '', 
    'Explanations': '', 
    "You still did not know what's inside the tower.||A secret building is hidden there.": '', 
    'The exhibition takes place in the tower.': '', 


    'LAN play': '', 


    '': '', 
    '': '', 
    '': '', 
    };





function tr(text) {
    var result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }


