/**
#-----------------------------------------------------------------
# This is a part of Collège 3D project.
# Author:       Pascal Peter
# Copyright:    (C) 2003-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


var TRANSLATIONS = {
    'GAME': 'JEU', 
    'CHARACTER': 'PERSONNAGE', 

    'CONTROLLER': 'CONTRÔLEUR', 
    'movements: arrows, WASD ----- jumps: space bar': 
        'déplacements : flèches, WASD ----- sauts : barre espace', 
    '|camera change: C': '|changement de caméra : C', 
    '|exit: Esc ----- pause: P ----- review last message: M': 
        '|quitter : Échap ----- pause : P ----- revoir le dernier message : M', 
    'movements: left-right, left bottom ----- jumps: right click': 
        'déplacements : gauche-droite, bouton gauche ----- sauts : clic droit', 
    '|camera change: scroll wheel, C': '|changement de caméra : molette, C', 
    'movements: ttt ----- jumps: ttt': 'déplacements : ttt ----- sauts : ttt', 
    '|camera change: ttt, C': '|changement de caméra : ttt, C', 
    'movements: axes ----- jumps: button 1': 'déplacements : axes ----- sauts : bouton 1', 
    '|camera change: button 2, C': '|changement de caméra : bouton 2, C', 
    '|exit: button 5, Esc ----- pause: button 6, P ----- review last message: button 3, M': 
        '|quitter : bouton 5, Échap ----- pause : bouton 6, P ----- revoir le dernier message : bouton 3, M', 

    'EFFECT': 'EFFET', 
    'anaglyph': 'anaglyphes', 
    'stereo': 'stéréo', 
    'ascii': 'ascii', 
    'outline': 'contour', 

    'LANG': 'LANGUE', 

    'SOUNDS': 'SONS', 
    'MUSIC': 'MUSIQUE', 
    'INFOS': 'INFOS', 

    'Help of the game': 'Aide du jeu', 
    'Project web page': 'Page web du projet', 
    'Reset to default settings': 'Remettre les réglages par défaut', 
    'PLAY': 'JOUER', 


    'Esc': 'Échap', 
    'exit': 'quitter', 
    'pause': '', 
    'review last message': 'revoir le dernier message', 
    'change camera': 'changer de caméra', 

    'LOADING...': 'CHARGEMENT...', 
    'LOADED !!!': 'CHARGÉ !!!', 
    'QUIT THE GAME ?': 'QUITTER LE JEU ?', 

    'keyboard': 'clavier', 
    'mouse': 'souris', 
    'touchscreen': 'écran tactile', 
    'gamepad': 'manette', 

    'nothing': 'aucun', 
    'student': 'élève', 
    'teacher': 'prof', 
    'dog': 'chien', 
    'tower': 'tour', 
    'robot': '', 
    'kangaroo': 'kangourou', 

    'LEVELS': 'NIVEAUX', 
    'Read the explanations before clicking on a level.': 
        'Lis bien les explications avant de cliquer sur un niveau.', 
    'Back to menu': 'Retour au menu', 
    'Level': 'Niveau', 
    'YOU FINISHED THIS GAME!': 'TU AS TERMINÉ CE JEU !', 
    'YOU LOST!': 'TU AS PERDU !', 


    'Free visit': 'Visite libre', 


    'Discover the college': 'Découvrir le collège', 
    'Before Enter': "Avant d'entrer", 
    "You'll find different places to college entrance.||Initially a marker will help you.": 
        "Tu devras trouver différents lieux à l'entrée du collège.||Au départ un repère t'aidera.", 
    'Go to the gate.': "Va jusqu'au portail.", 
    'Go under the bike garage.': 'Va sous le garage à vélos.', 
    'Go to the front door.': "Va jusqu'à la porte d'entrée.", 
    'The Hall': 'Sous le hall', 
    'Everything happens this time under the hall.': 'Tout se passe cette fois sous le hall.', 
    'Go between the 2 fountains.': 'Va entre les 2 fontaines.', 
    'Go in front of the reception.': "Va devant l'accueil.", 
    'Go to the exit of the refectory.': 'Va à la sortie du réfectoire.', 
    'Near the tower': 'Près de la tour', 
    'Because there is a tower in the school yard.': 'Car il y a une tour dans la cour du collège.', 
    'Go between the ping-pong tables.': 'Va entre les tables de ping-pong.', 
    'Go to the tower.': 'Va à la tour.', 
    'Go to the entrance of the cafeteria.': "Va à l'entrée de la cantine.", 
    'The buildings': 'Les bâtiments', 
    'You will know the building entrances.': 'Tu connaîtras les entrées des bâtiments.', 
    'Go under the small hall.|(building A)': 'Va sous le petit hall.|(bâtiment A)', 
    'Go to the stairs of building A.': 'Va devant les escaliers du bâtiment A.', 
    'Go to the entrance of Building B.': "Va à l'entrée du bâtiment B.", 
    'Go to the entrance of the CDI.|(building C)': "Va à l'entrée du CDI.|(bâtiment C)", 
    'Go to the entrance of the technology rooms.|(building C)': 
        "Va à l'entrée des salles de technologie.|(bâtiment C)", 


    'During seasons': 'Au cours des saisons', 


    '2003-2013': '', 


    'The exhibition': "L'exposition", 
    'Explanations': 'Explications', 
    "You still did not know what's inside the tower.||A secret building is hidden there.": 
        "Tu ne savais pas encore ce qu'il y a à l'intérieur de la tour.||Un bâtiment secret y est caché.", 
    'The exhibition takes place in the tower.': "L'exposition a lieu dans la tour.", 


    'LAN play': 'Jouer en réseau', 


    '': '', 
    '': '', 
    '': '', 
    };





function tr(text) {
    var result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }


